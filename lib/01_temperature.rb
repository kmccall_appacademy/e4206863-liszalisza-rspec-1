# A set of functions to convert from fahrenheit to celsius and the other way around

def ftoc(degree_fahrenheit)
  (degree_fahrenheit - 32) * 5/9
end

def ctof(degree_celsius)
  degree_celsius.to_f * 9/5 + 32
end
