# Some calculator methods
def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def sum(arr)
  arr.reduce(0,:+)
end

def multiply(arr)
  arr.reduce(1, :*)
end

def power(num, power)
  num**power
end

def factorial(num)
  return 1 if num <= 1
  factorial(num-1) * num
end
