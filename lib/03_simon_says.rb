# Simon Says methods
require 'byebug'

def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, num = 2)
  arr = []
  num.times {arr << str}
  arr.join(" ")
end

def start_of_word(str, num_letters = 1)
  str[0...num_letters]
end

def first_word(str)
  str.split(" ").first
end

def titleize(str)
  little_words = ["a", "an", "the", "and", "but", "or", "for", "nor", "on", "at", "to", "from", "by", "over"]

  str.split.map.with_index do |word, i|
    if little_words.include?(word) && i != 0
      word.downcase
    else
      word.capitalize
    end    
  end.join(" ")
end
