# Pig Latin
def translate(str)
  pig_tranlation = str.split. map do |word|
    i = 0
    while consonant?(word[i])
      if word[i,2] == "qu"
        i += 2
      else
        i += 1
      end
    end

    word[i..-1] + word[0,i] + "ay"
  end
  pig_tranlation.join(" ")
end

def consonant?(letter)
  vowels = "aeiou"
  !vowels.include?(letter)
end
